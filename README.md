# ModalProvider POC

## POC content

Please note that all of the code is documented inside the files.

* [ModalProvider initial implementation](./src/App/MadalProvider.jsx)

Example code

* [Example application](./src/App/index.jsx)
* [ModalComponent example](./src/App/ModalComponent.jsx)


## How to run

The following commands run the devserver at http://localhost:3000

Please note that because this application uses [materializecss](https://materializecss.com/) from CDN, the internet connection is necessary to get the correct representation of this example.

```sh
git clone https://gitlab.com/Hojyman/mx.poc.modal.git
cd mx.poc.modal
npm ci
npm start
```

## Avaliable NPM scripts

`npm start` - webpack-dev-server

`npm run build` - build production artifact

`npm run esfix` - check and fix the code with eslint

