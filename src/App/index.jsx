/* eslint-disable
  react/prop-types,
  jsx-a11y/no-static-element-interactions,
  jsx-a11y/click-events-have-key-events,
  react/button-has-type
*/

import React from 'react';
import ReactDOM from 'react-dom';

import { ModalProvider, modal, withModal } from './MadalProvider';

/*
  This component will be shown only while the modal controller show method
  is called and its isOpen property becomes true.

  While shown, it git two properties: `config` and `hide` from the modal
  controller.

  The config is the one used when the `show` method was fired.
  It is not used by the controller itself and the only purpose of it is to
  transfer some properties from the method firing point to the ModalComponent.

  The `hide` property is the method which has been fired by the ModalComponent
  to make it closed. The method takes no arguments.
 */
import ModalComponent from './ModalComponent';

/*
  The modal controller provides the subscribe method which allows some listener
  to get notified when the state of the modal component has changed its value
  (isOpen become true or false).

  The listener got two arguments: `isOpen` state as the first and the basic
  modal controller (getConfig, show, hide, subscribe) as the second one.
 */
/* eslint-disable-next-line */
modal.subscribe((isOpen, modal) => console.log({ isOpen, modal }));

/*
  The config to use with the `modal.show(config)`.

  Note that the config is not used by the controller itself and the only purpose
  of it is to transfer some properties from the method firing point to the custom
  ModalComponent which is a part of the application (mot the modal provider).
 */
const modalConfig = {
  title: 'Modal Header',
  text: 'A bunch of text',
};

/*
  The `withModal` decorator supplies one additional property `modal`. This property
  contains basic model controlled (`getConfig`, `show`, `hide`, `subscribe`)
  alongside an additional property isOpen (true/false) which reflects the current
  modal status.
 */
const AnyComponent = withModal(({ modal: mX }) => (
  <button
    className="waves-effect waves-light btn"
    onClick={
        /*
          We use global modal controller here, to show the popup.

          Note that the local controller obtained via props (mX here) can be used
          in the same way.
        */
        () => modal.show(modalConfig)
      }
    disabled={
        /*
          We use the modal controller of props (`getConfig`, `show`, `hide`, `subscribe`,
          `isOpen`) obtained from withModal decorator to get isOpen state.

          Note that this state can be also obtained with the `getIsOpen` method of
          the global modal controller too. But it cannot be used to fire the rendering
          cycle.
         */
        mX.isOpen
      }
  >
    Open modal
  </button>
));

function MainView() {
  return <AnyComponent />;
}

function App() {
  return (
    <div className="container">
      <MainView />
      {
        /*
          ModalProvider defines a DOM place where the ModalComponent will be rendered when
          shown.

          Note that placing it after the other parts of the applications view in the HTML
          flow, makes sure that it will be placed "above" these parts on the page by default.
         */
      }
      <ModalProvider component={ModalComponent} />
    </div>
  );
}

ReactDOM.render(
  <App />,
  document.getElementById('root'),
);
