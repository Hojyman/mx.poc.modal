import React from 'react';

/* eslint-disable
  react/prop-types,
  jsx-a11y/no-static-element-interactions,
  jsx-a11y/click-events-have-key-events,
  react/button-has-type
*/

const modalStyles = {
  viewCover: {
    display: 'block',
    position: 'absolute',
    top: 0,
    left: 0,
    height: '100vh',
    width: '100vw',
    paddingTop: 20,
    // pointerEvents:'none',

    background: 'rgba(0, 0, 0, 0.4)',
    zIndex: 9999,
  },

  modal: {
    display: 'block',
  },
};

export default function MyModal({ config, hide }) {
  return (
    <div style={modalStyles.viewCover}>
      <div className="modal" style={modalStyles.modal}>
        <div className="modal-content">
          <h4>{ config.title }</h4>
          <p>{ config.text }</p>
        </div>
        <div className="modal-footer">
          <button
            onClick={hide}
            className="modal-close waves-effect waves-green btn-flat"
          >
            Agree
          </button>
        </div>
      </div>
    </div>
  );
}
