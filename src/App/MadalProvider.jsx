// ModalProvider.jsx
// -----------------------
/* eslint-disable
  react/prop-types,
  react/no-this-in-sfc,
  react/jsx-props-no-spreading
 */

import React from 'react';

function Observable() {
  const observers = [];

  this.subscribe = (observer) => {
    observers.push(observer);
    const unsubscribe = () => {
      observers
        .some((x, idx) => {
          if (x === observer) {
            observers.splice(idx, 1);
            return true;
          }
          return false;
        });
    };
    return unsubscribe;
  };

  this.dispatch = (...values) => {
    observers.forEach((observer) => observer(...values));
  };
}

function Modal() {
  const o = new Observable();
  let isOpen = false;
  let config = {};
  const setIsOpen = (v) => {
    isOpen = v;
    o.dispatch(v, this.getModalBase());
  };

  this.getConfig = () => config;

  this.hide = () => {
    config = {};
    setIsOpen(false);
  };

  this.show = (cfg) => {
    config = cfg;
    setIsOpen(true);
  };

  this.subscribe = o.subscribe;

  this.getIsOpen = () => isOpen;

  this.getModalBase = () => ({
    getConfig: this.getConfig,
    hide: this.hide,
    show: this.show,
    subscribe: this.subscribe,
  });

  this.ModalProvider = ({ component: ModalComponent }) => {
    const [_isOpen, _setIsOpen] = React.useState(false);
    o.subscribe(_setIsOpen);

    if (!_isOpen) { return null; }
    return <ModalComponent config={this.getConfig()} hide={this.hide} />;
  };

  this.withModal = (Component) => (ownProps) => {
    const forceUpdate = React.useReducer((x) => x + 1, 0)[1];
    o.subscribe(forceUpdate);
    return (
      <Component
        {...ownProps}
        modal={{
          ...this.getModalBase(),
          isOpen,
        }}
      />
    );
  };
}

const modalProvider = new Modal();

/**
  * The modal controller can be used whenever in the application (inside and outside
  * of the react scope both) to whos/hide modal window, get its state with the
  * `getIsOpen` method and subscribe for the modal visibility state change (use
  * `subscribe` method for it).
  *
  * @type {Object}
  *
  * It supplies the following methods:
  *
  *   getConfig() - returns the currently applied modal config (while the modal is
  *     visible).
  *
  *   hide() - allows to hide the currently opened modal popup.
  *
  *   show(config) - allows showing modal popup with the given config.
  *
  *   subscribe(listener) - allows the listener to get notified each time when the
  *     visibility of the modal is changed. The listener here got two arguments:
  *     visibility state as the first and mobile controller as the second.
  *
  *   getIsOpen() - allows to get the current state of the modal popup's visibility
  *     (true/false)
  */
export const modal = {
  ...modalProvider.getModalBase(),
  getIsOpen: modalProvider.getIsOpen,
};

export const {
  /**
   * The `withModal` decorator allows a react component to get notified each time when
   * the modal visibility state is changed.
   *
   * This decorator supplies one additional property `modal` to the wrapped component.
   * This property contains basic model controlled (`getConfig`, `show`, `hide`,
   * `subscribe`) alongside an additional property isOpen (true/false) which reflects
   * the current modal status.
   *
   * @example
   *   withModal(({ modal }) => { return modal.isOpen ? 'Shown' : 'Hidden' })
   */
  withModal,

  /**
   * ModalProvider defines a DOM place where the ModalComponent will be rendered when
   * shown. It takes the only specific property `component` which has to point a
   * ModalComponent to render while modal should be shown.
   *
   * Note that placing it after the other parts of the applications view in the HTML
   * flow, makes sure that it will be placed "above" these parts on the page by default.
   *
   * @example
   *   <ModalProvider component={ModalComponent} />
   */
  ModalProvider,
} = modalProvider;

export default modalProvider.ModalProvider;
