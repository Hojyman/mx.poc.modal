const path = require('path');

const rootFolder = path.resolve('./');

const config = {
  presets: [
    ['@babel/preset-env',
      {
        loose: true,
      },
    ],
    '@babel/preset-react',
  ],
  plugins: [
    ['@babel/plugin-proposal-decorators', { legacy: true }],
    ['@babel/plugin-proposal-class-properties', { loose: true }],
    // NOTE: (kard) it looks like we have this plugin in presets
    // '@babel/plugin-proposal-object-rest-spread',
    [
      // require.resolve('babel-plugin-module-resolver'),
      'babel-plugin-module-resolver',
      {
        root: [rootFolder],
        alias: {
          // '@kard/react-app-context': path.join(rootFolder, './packages/@kard/react-app-context'),
          '@kard': path.join(rootFolder, './packages/@kard'),
        },
      },
    ],
  ],
  env: {
    test: {
      // plugins: [
      //   '@babel/plugin-transform-modules-commonjs',
      // ],
    },
  },
};

/* eslint-disable func-names */
module.exports = function (api) {
  if (api && api.cache) {
    api.cache(true);
  }
  return config;
};
