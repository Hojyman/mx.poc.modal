/* eslint-disable no-underscore-dangle */

process.env.BABEL_ENV = 'development';
process.env.NODE_ENV = 'development';

const baseConfig = require('./base.config');

const createConfig = (env, argv) => {
  const _createConfig = baseConfig({
    /* put developmant params here */
  });
  const _config = _createConfig(env, argv);
  _config.watch = true; // make webpack watch for changes

  _config.devServer = {
    // contentBase: path.join(__dirname, 'dist'),
    // compress: true,
    port: 3000,
    // headers: {
    //   'Access-Control-Allow-Origin': '*',
    //   'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
    //   'Access-Control-Allow-Headers': 'X-Requested-With, content-type, Authorization',
    // },
  };

  return _config;
};

module.exports = createConfig;
